﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameManager : MonoBehaviour {

	public Transform dungeonParent;
	public Transform playerObject;
	private void Start()
	{
		ResetTime();	
		DungeonGenerator.seed = (int)Time.time;
		DungeonGenerator.create();
		StartCoroutine(WaitTillDungeonCreate());
	}

	IEnumerator WaitTillDungeonCreate()
	{
		while(!DungeonGenerator.levelGenerated)
			yield return new WaitForEndOfFrame();
		PositionPlayer();
	}

	public void Pause()
	{
		Time.timeScale = 0f;
	}

	public void ResetTime()
	{
		Time.timeScale = 1.0f;
	}
	public void Restart()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	private void PositionPlayer()
	{
		dungeonParent = GameObject.Find("Dungeon").transform;
		playerObject.position = dungeonParent.GetChild(0).position+Vector3.up*1.0f;
	}
}
