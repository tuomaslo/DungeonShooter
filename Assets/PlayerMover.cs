﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMover : MonoBehaviour {

	public float rayLength = 1.0f;
	
	public bool isMoving = false;

	// Use this for initialization	
	void Start () {
		isMoving = false;	
	}
	
	// Update is called once per frame
	void Update () {
		//Debug.DrawRay(transform.position, transform.forward*rayLength);
	}

	public void Move(int direction)
	{	
		if(!isMoving)
		{
			//1 forward
			//-1 backward
			if(CanMove(direction))
			{
				StartCoroutine(Movement(direction));
			}
			else
			{
				Debug.Log("CANNOT MOVE TO DIRECTION "+direction);
			}
		}
	}

	private bool CanMove(int direction)
	{
		 // Bit shift the index of the layer (8) to get a bit mask
       // int layerMask = 1 << 8;

        // This would cast rays only against colliders in layer 8.
        // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
        //layerMask = ~layerMask;

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.forward*direction, out hit, rayLength))//, layerMask))
        {
            Debug.Log("Did Hit");
			return false;
        }
        else
        {
            Debug.Log("Did not Hit");
			return true;
        }
		
	}

	public void Turn(int direction)
	{
		//-1 Left 
		//1 Right
		if(!isMoving)
		{
			StartCoroutine(Rotate(Vector3.up, direction*90f, 1.0f));	
		}
	}

	
   IEnumerator Rotate( Vector3 axis, float angle, float duration = 1.0f)
   {
		isMoving = true;
		Quaternion from = transform.rotation;
		Quaternion to = transform.rotation;
		to *= Quaternion.Euler( axis * angle );
		
		float elapsed = 0.0f;
		while( elapsed < duration )
		{
		transform.rotation = Quaternion.Slerp(from, to, elapsed / duration );
		elapsed += Time.deltaTime;
		yield return null;
		}
		transform.rotation = to;
		isMoving = false;
   }
	
	IEnumerator Movement(int direction, float duration = 1.0f)
	{
		isMoving = true;
		Vector3 from = transform.position;
		Vector3 to = transform.position+(transform.forward*direction*5f);

		float elapsed = 0.0f;
		while( elapsed < duration )
		{
			transform.position = Vector3.Lerp(from, to, elapsed / duration );
			elapsed += Time.deltaTime;
			yield return null;
		}
		transform.position = to;
		isMoving = false;
	}
}
