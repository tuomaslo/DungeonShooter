﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class MapTileModule : MonoBehaviour {
	public Type type = Type.normal;
	public List<GameObject> startTileList = new List<GameObject>();
	public List<GameObject> endTileList = new List<GameObject>();
	public List<GameObject> walls = new List<GameObject>();
	private Vector2 position;
	public int left,right,up,down;
	public Vector4 neighbours;
	public enum Type
	{
		normal, start, end
	}	

	public int objectType = 0;
	public int rotation = 0;

	public void Initialize(int typeInt, Vector2 position, int rotation)
	{
		switch(typeInt)
		{
			case 0:
				type = Type.normal;
				break;
			case 1:
				type = Type.start;
				gameObject.name = "START";
				break;
			case 2:
				type = Type.end;
				gameObject.name = "END";
				break;
		}
		this.position = position;
		this.rotation = rotation/90;
		neighbours = DungeonGenerator.GetNeighbours(position);
		CheckWalls();
		EnableModules();
	}

	private void EnableModules()
	{
		
		switch(type)
		{
			case Type.start:
				/*foreach(GameObject g in startTileList)
				{
					g.SetActive(true);
				}*/
				
				break;
			case Type.end:
				foreach(GameObject g in endTileList)
				{
					g.SetActive(true);
				}
				break;
		}
	}
	
	private void CheckWalls()
	{
		int i = 0;

		if(type==Type.start || type == Type.end)
		{
			if(neighbours.x == -1)
			{
				if(!walls[i].activeInHierarchy)
				{
					Debug.Log(type.ToString()+" x enabled");
					walls[i].SetActive(true);
				}
			}
			if(neighbours.y == -1)
			{
				if(!walls[i+1].activeInHierarchy)
				{
					Debug.Log(type.ToString()+" y enabled");
					walls[i+1].SetActive(true);
				}
			}
			if(neighbours.z == -1)
			{
				if(!walls[i+2].activeInHierarchy)
				{
					Debug.Log(type.ToString()+" z enabled");
					walls[i+2].SetActive(true);
				}
			}
			if(neighbours.w == -1)
			{
				if(!walls[i+3].activeInHierarchy)
				{
					Debug.Log(type.ToString()+" w enabled");
					walls[i+3].SetActive(true);
				}
			}

		}
	}

	private void NormalizeRotationNeighbours()
	{
		Vector4 tempVector = neighbours;
		Vector4 temp2Vector = Vector4.zero;
		for(int i = 0;i<rotation;i++)
		{
			temp2Vector[0] = tempVector[3];
			temp2Vector[1] = tempVector[0];
			temp2Vector[2] = tempVector[1];
			temp2Vector[3] = tempVector[2];
		}
		neighbours = temp2Vector;
	}
}
